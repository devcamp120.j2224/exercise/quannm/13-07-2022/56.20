package com.devcamp.s50.task5620;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class J5620Application {

	public static void main(String[] args) {
		SpringApplication.run(J5620Application.class, args);
	}

}
