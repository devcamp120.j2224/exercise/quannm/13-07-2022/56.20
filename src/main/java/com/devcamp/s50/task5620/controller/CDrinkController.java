package com.devcamp.s50.task5620.controller;

import com.devcamp.s50.task5620.model.CDrink;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;

@RestController
public class CDrinkController {
    @CrossOrigin
    @GetMapping("/devcamp-drinks")
    public ArrayList<CDrink> getDrink() {
        ArrayList<CDrink> listDrink = new ArrayList<CDrink>();
        LocalDate today = LocalDate.now(ZoneId.systemDefault());

        CDrink tratac = new CDrink(1, "TRATAC", "Trà tắc", 10000, today, today);
        CDrink coca = new CDrink(2, "COCA", "Cocacola", 15000, today, today);
        CDrink pepsi = new CDrink(3, "PEPSI", "Pepsi", 15000, today, today);

        listDrink.add(tratac);
        listDrink.add(coca);
        listDrink.add(pepsi);

        return listDrink;
    }
}
